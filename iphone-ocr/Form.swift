//
//  Form.swift
//  iPhoneOCR
//
//  Created by Masaya Iwasaki on 2019/01/15.
//  Copyright © 2019年 Masaya Iwasaki. All rights reserved.
//

import UIKit
import os.log

//MARK: Types
struct PropertyKey {
    static let name = "name"
    static let photo = "photo"
    static let ocr_result = "ocr_result"
}

class Form: NSObject, NSCoding {
    //MARK: Properties
    var name: String
    var photo: UIImage?
    var ocr_result: String

    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("forms")

    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(photo, forKey: PropertyKey.photo)
        aCoder.encode(ocr_result, forKey: PropertyKey.ocr_result)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        // The name is required. If we cannot decode a name string, the initializer should fail.
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            os_log("Unable to decode the name for a Form object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        // Because photo is an optional property of Form, just use conditional cast.
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as? UIImage
        
        guard let ocr_result = aDecoder.decodeObject(forKey: PropertyKey.ocr_result) as? String else {
            os_log("Unable to decode the name for a Form object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        // Must call designated initializer.
        self.init(name: name, photo: photo, ocr_result: ocr_result)
        
    }
    
    
    //MARK: Initialization
    
    init?(name: String, photo: UIImage?, ocr_result: String) {
        
        // The name must not be empty
        guard !name.isEmpty else {
            return nil
        }
        
        // Initialize stored properties.
        self.name = name
        self.photo = photo
        self.ocr_result = ocr_result
        
    }
}
