//
//  FoodTrackerTests.swift
//  FoodTrackerTests
//
//  Created by Masaya Iwasaki on 2019/01/10.
//  Copyright © 2019年 Masaya Iwasaki. All rights reserved.
//

import XCTest
@testable import FoodTracker


class FoodTrackerTests: XCTestCase {
    //MARK: Form Class Tests

    // Confirm that the Form initializer returns a Form object when passed valid parameters.
    func testFormInitializationSucceeds() {
        // Zero rating
        let zeroRatingForm = Form.init(name: "Zero", photo: nil, rating: 0)
        XCTAssertNotNil(zeroRatingForm)
        
        // Highest positive rating
        let positiveRatingForm = Form.init(name: "Positive", photo: nil, rating: 5)
        XCTAssertNotNil(positiveRatingForm)
        
    }

    // Confirm that the Meal initialier returns nil when passed a negative rating or an empty name.
    func testFormInitializationFails() {
        // Negative rating
        let negativeRatingForm = Form.init(name: "Negative", photo: nil, rating: -1)
        XCTAssertNil(negativeRatingForm)
        
        // Empty String
        let emptyStringMeal = Form.init(name: "", photo: nil, rating: 0)
        XCTAssertNil(emptyStringMeal)
        
        // Rating exceeds maximum
        let largeRatingMeal = Form.init(name: "Large", photo: nil, rating: 6)
        XCTAssertNil(largeRatingMeal)
        
    }
}

