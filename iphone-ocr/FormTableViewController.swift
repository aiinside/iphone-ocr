//
//  FormTableViewController.swift
//  iPhoneOCR
//
//  Created by Masaya Iwasaki on 2019/01/15.
//  Copyright © 2019年 Masaya Iwasaki. All rights reserved.
//

import UIKit
import os.log

class FormTableViewController: UITableViewController {
    
    //MARK: Actions
    @IBAction func unwindToFormList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? FormViewController, let form = sourceViewController.form {
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                // Update an existing form.
                forms[selectedIndexPath.row] = form
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }
            else {

                // Add a new form.
                let newIndexPath = IndexPath(row: forms.count, section: 0)
                
                forms.append(form)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
            // Save the forms.
            saveForms()
        }
    }
    
    //MARK: Properties
    
    var forms = [Form]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Use the edit button item provided by the table view controller.
        navigationItem.leftBarButtonItem = editButtonItem
        // loadSampleForms()

        // Load any saved forms, otherwise load sample data.
        if let savedForms = loadForms() {
            forms += savedForms
        }
        else {
            // Load the sample data.
            loadSampleForms()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forms.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "FormTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? FormTableViewCell  else {
            fatalError("The dequeued cell is not an instance of FormTableViewCell.")
        }
        
        // Fetches the appropriate form for the data source layout.
        let form = forms[indexPath.row]
        
        cell.nameLabel.text = "[" + form.name + "]"
        cell.photoImageView.image = form.photo
        cell.ocrResutLabel.text = form.ocr_result
        
        return cell
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            forms.remove(at: indexPath.row)
            saveForms()
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
         
     // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
            
        case "AddItem":
            os_log("Adding a new form.", log: OSLog.default, type: .debug)
            
        case "ShowDetail":
            guard let formDetailViewController = segue.destination as? FormViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedFormCell = sender as? FormTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedFormCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedForm = forms[indexPath.row]
            formDetailViewController.form = selectedForm
            
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
    
    //MARK: Private Methods
    
    private func loadSampleForms() {
        
        let photo1 = UIImage(named: "image1")
        
        guard let form1 = Form(name: "氏名", photo: photo1, ocr_result: "test") else {
            fatalError("Unable to instantiate form1")
        }

//        guard let form2 = Form(name: "アンケート", photo: photo2, ocr_result: "test") else {
//            fatalError("Unable to instantiate form2")
//        }
//
//        guard let form3 = Form(name: "レシート", photo: photo3, ocr_result: "test") else {
//            fatalError("Unable to instantiate form3")
//        }
        
        forms += [form1]
    }
    
    private func saveForms() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(forms, toFile: Form.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Forms successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save forms...", log: OSLog.default, type: .error)
        }
    }
    
    private func loadForms() -> [Form]?  {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Form.ArchiveURL.path) as? [Form]
    }
}
