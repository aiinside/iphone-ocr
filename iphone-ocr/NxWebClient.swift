//
//  NxWebClient.swift
//  iphone-ocr
//
//  Created by Masaya Iwasaki on 2019/02/04.
//  Copyright © 2019年 Masaya Iwasaki. All rights reserved.
//

import Foundation
import Alamofire

class NxWebClient{
    class func myImageUploadRequest(imageView: UIImageView, ocrResultField: UITextView){
        let url = "https://ocr-api.inside.ai/NxWeb/v1/NxRead/katsuji"
        let apiKey = "test-api-key"
        let imageData = UIImageJPEGRepresentation(imageView.image!, 1.0)!

        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(apiKey.data(using: .utf8)!, withName: "apiKey")
                multipartFormData.append(imageData, withName: "image")
        },
            to: url,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        print("my response")
                        print(response)
                        if let JSON = response.result.value as? [String: Any] {
                            ocrResultField.text = JSON["Text"] as! String
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        )
    }
    
}
