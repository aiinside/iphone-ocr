//
//  FormTableViewCell.swift
//  iPhoneOCR
//
//  Created by Masaya Iwasaki on 2019/01/15.
//  Copyright © 2019年 Masaya Iwasaki. All rights reserved.
//

import UIKit

class FormTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ocrResutLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
